import 'dart:math';

import 'package:devol_chart/widgets/devol_chart/devol_chart.dart';
import 'package:devol_chart/widgets/devol_chart/model/chart_line_params.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Devol chart',
      home: const MyHomePage(title: 'Devol chart'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF17181D),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const Text('Chart test'),
            ),
            Expanded(
              child: DevolChart(
                lines: [
                  ChartLineParams(
                    color: Colors.blue,
                    decimals: 1,
                    points: [
                      for (var i = 0; i < 40; i++)
                        ChartPoint(i, pow(i, 2) - i - 400),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
