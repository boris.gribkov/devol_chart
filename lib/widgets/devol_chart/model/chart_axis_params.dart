import 'package:flutter/material.dart';

enum XAxisType { date, number }

class ChartAxisParams {
  final int decimals;
  final TextStyle textStyle;
  final Color color;

  const ChartAxisParams({
    this.decimals = 2,
    this.textStyle = const TextStyle(color: Colors.grey),
    this.color = Colors.grey,
  });
}

class ChartXAxisParams extends ChartAxisParams {
  final XAxisType xAxisType;

  const ChartXAxisParams({
    this.xAxisType = XAxisType.number,
    super.decimals,
    super.textStyle,
    super.color,
  });
}

class ChartYAxisParams extends ChartAxisParams {
  const ChartYAxisParams({
    super.decimals,
    super.textStyle,
    super.color,
  });
}

class ChartAxesParams {
  final ChartYAxisParams chartYAxisParams;
  final ChartXAxisParams chartXAxisParams;

  const ChartAxesParams({
     this.chartYAxisParams = const ChartYAxisParams(),
     this.chartXAxisParams = const ChartXAxisParams(),
  });
}
