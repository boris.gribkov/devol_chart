import 'dart:math';

import 'package:flutter/material.dart';

class ChartPoint {
  final num x;
  final num y;

  ChartPoint(this.x, this.y);

  @override
  String toString() => 'Point($x, $y)';

  @override
  bool operator ==(Object other) {
    return other is Point && x == other.x && y == other.y;
  }

  @override
  int get hashCode => Object.hash(x.hashCode, y.hashCode, 0);
}

extension DateTimeCompareOperator on DateTime {
  bool operator <(DateTime other) {
    return isBefore(other);
  }

  bool operator >(DateTime other) {
    return isBefore(other);
  }
}

class ChartLimits {
  num? maxX;
  num? minX;
  num? maxY;
  num? minY;

  ChartLimits({this.minY, this.maxY, this.minX, this.maxX});

  ChartLimits.fromPoints(List<ChartPoint> points) {
    // Find max and min x
    for (var i = 0; i < points.length; ++i) {
      if (maxX == null || points[i].x > maxX!) {
        maxX = points[i].x;
      }
      if (minX == null || points[i].x < minX!) {
        minX = points[i].x;
      }
    }
    // Find max and min y
    for (var i = 0; i < points.length; ++i) {
      if (maxY == null || points[i].y > maxY!) {
        maxY = points[i].y;
      }
      if (minY == null || points[i].y < minY!) {
        minY = points[i].y;
      }
    }
  }
}

enum YMarksPosition { left, right }
enum XMarksPosition { top, bottom }

/// TypeX - x axis type
class ChartLineParams {
  final YMarksPosition yMarksPosition;
  final XMarksPosition xMarksPosition;
  final int decimals;
  final double width;
  final Color color;
  final List<ChartPoint> points;
  ChartLimits? _chartLimits;

  ChartLimits get chartLimits => _chartLimits ?? ChartLimits();

  ChartLineParams({
    required this.decimals,
    required this.color,
    required this.points,
    this.width = 1,
    this.xMarksPosition = XMarksPosition.bottom,
    this.yMarksPosition = YMarksPosition.right,
    ChartLimits? chartLimits,
  }) {
    _chartLimits = _findLimits(points, chartLimits);
  }

  ChartLimits _findLimits(
    List<ChartPoint> points,
    ChartLimits? chartLimits,
  ) {
    final limits = ChartLimits.fromPoints(points);
    if (chartLimits != null) {
      if (chartLimits.maxY != null) {
        limits.maxY = chartLimits.maxY;
      }
      if (chartLimits.minY != null) {
        limits.minY = chartLimits.minY;
      }
      if (chartLimits.maxX != null) {
        limits.maxX = chartLimits.maxX;
      }
      if (chartLimits.minX != null) {
        limits.minX = chartLimits.minX;
      }
    } else {
      _chartLimits = limits;
    }
    return limits;
  }
}
