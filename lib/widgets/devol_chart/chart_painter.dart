import 'dart:math';
import 'dart:ui';

import 'package:devol_chart/widgets/devol_chart/model/chart_axis_params.dart';
import 'package:devol_chart/widgets/devol_chart/model/chart_line_params.dart';
import 'package:flutter/material.dart';

part 'draw_helpers.dart';

const double leftPadding = 1;
const double rightPadding = 50;
const double topPadding = 1;
const double bottomPadding = 30;

final lightChartTextStyle = TextStyle(color: Colors.green);
final lightChartValueTrackingTextStyle = TextStyle(color: Colors.white);
final mainBackgroundColor = Colors.grey.shade800;
final windowBorderColor = Colors.grey.shade700;

const double _textFramePadding = 8;

class DevolChartPainter extends CustomPainter {
  DevolChartPainter({
    required this.lines,
    required this.axesParams,
    this.cursor,
  });

  final Point<double>? cursor;
  final List<ChartLineParams> lines;
  final ChartAxesParams axesParams;

  @override
  void paint(Canvas canvas, Size size) {
    final canvasSize = size;

    // Paint params
    final testAxes = Paint()
      ..color = Colors.yellow
      ..strokeWidth = 2;
    final paintAxes = Paint()
      ..color = axesParams.chartXAxisParams.color
      ..strokeWidth = 0;
    final paintAxesDot = Paint()
      ..color = axesParams.chartYAxisParams.color
      ..strokeWidth = 0;

    final chartPosition = Offset(leftPadding, topPadding);
    final chartSize = Size(
      canvasSize.width - chartPosition.dx - rightPadding,
      canvasSize.height - chartPosition.dy - bottomPadding,
    );
    final chartEdges = Rect.fromPoints(
      chartPosition,
      Offset(
        chartPosition.dx + chartSize.width,
        chartPosition.dy + chartSize.height,
      ),
    );

    for (final line in lines) {
      var decimals = line.decimals;

      // Text padding
      int textPadding = 4; // TODO

      // Paint line params
      final paintLine = Paint()
        ..color = line.color
        ..strokeWidth = line.width;

      // Draw axes lines
      canvas.drawLine(chartEdges.bottomLeft, chartEdges.bottomRight, paintAxes);
      canvas.drawLine(chartEdges.topRight, chartEdges.bottomRight, paintAxes);

      // Limits
      final limits = line.chartLimits;
      final minX = limits.minX ?? 0;
      final maxX = limits.maxX ?? 0;
      final minY = limits.minY ?? 0;
      final maxY = limits.maxY ?? 0;

      // Calculate grid step
      final gridStep = Point(
        (maxX - minX) / 5,
        (maxY - minY) / 8,
      );

      // Draw horizontal grid
      // If line is on positive and negative Y side, draw line and place text 0
      if (minY < 0 && maxY > 0) {
        final posY = chartEdges.height * (maxY - 0) / (maxY - minY);
        final span = TextSpan(
            style: lightChartTextStyle,
            text: 0.toStringAsFixed(decimals).padLeft(textPadding));
        drawHorizontalGridLineWithText(
          canvas,
          chartEdges,
          posY,
          span,
          paintAxes,
          decimals,
          textPadding,
          dashed: false,
        );
      }
      // Grid above 0
      for (var y = max(0, minY) + gridStep.y; y < maxY; y += gridStep.y) {
        final posY = chartEdges.height * (maxY - y) / (maxY - minY);
        final span = TextSpan(
            style: lightChartTextStyle,
            text: y.toStringAsFixed(decimals).padLeft(textPadding));
        drawHorizontalGridLineWithText(canvas, chartEdges, posY, span,
            paintAxesDot, decimals, textPadding);
      }
      // Grid below 0
      for (var y = -gridStep.y; y > minY; y -= gridStep.y) {
        final span = TextSpan(
            style: lightChartTextStyle,
            text: y.toStringAsFixed(decimals).padLeft(textPadding));
        final posY = chartEdges.height * (maxY - y) / (maxY - minY);
        drawHorizontalGridLineWithText(canvas, chartEdges, posY, span,
            paintAxesDot, decimals, textPadding);
      }

      // Draw vertical grid
      for (var x = minX + gridStep.x; x < maxX; x += gridStep.x) {
        final posX = chartEdges.width * (x - minX) / (maxX - minX);

        final span = TextSpan(style: lightChartTextStyle, text: x.toString());
        drawVerticalGridLineWithText(canvas, chartEdges, posX, span,
            paintAxesDot, decimals, textPadding);
      }

      // Draw line
      for (var i = 1; i < line.points.length; ++i) {
        final offsetStart = Offset(
          chartEdges.width * (line.points[i - 1].x - minX) / (maxX - minX),
          chartEdges.height * (maxY - line.points[i - 1].y) / (maxY - minY),
        );
        final offsetEnd = Offset(
          chartEdges.width * (line.points[i].x - minX) / (maxX - minX),
          chartEdges.height * (maxY - line.points[i].y) / (maxY - minY),
        );
        canvas.drawLine(offsetStart, offsetEnd, paintLine);
      }

      if (cursor != null) {
        final dx = cursor!.x;
        final dy = cursor!.y;
        if (dx > leftPadding &&
            dy > topPadding &&
            dx < canvasSize.width - rightPadding &&
            dy < canvasSize.height - bottomPadding) {
          drawTracking(canvas, dx, dy, size);
          final fillLabel = Paint()
            ..color = mainBackgroundColor
            ..style = PaintingStyle.fill;
          final strokeParams = Paint()
            ..color = windowBorderColor
            ..style = PaintingStyle.stroke;
          // draw right focused value
          TextSpan span;
          span = TextSpan(
              style: lightChartValueTrackingTextStyle,
              text: (maxY -
                      (maxY - minY) * ((dy - topPadding) / canvasSize.height))
                  .toStringAsFixed(decimals));
          final tp = TextPainter(
              text: span,
              textAlign: TextAlign.left,
              textDirection: TextDirection.ltr)
            ..layout();
          canvas
            ..drawRect(
              Rect.fromPoints(
                Offset(canvasSize.width - rightPadding + 3,
                    dy - _textFramePadding - tp.height / 2),
                Offset(
                    canvasSize.width -
                        rightPadding +
                        3 +
                        tp.width +
                        _textFramePadding * 2,
                    dy + _textFramePadding + tp.height / 2),
              ),
              fillLabel,
            )
            ..drawRect(
              Rect.fromPoints(
                Offset(canvasSize.width - rightPadding + 3,
                    dy - _textFramePadding - tp.height / 2),
                Offset(
                    canvasSize.width -
                        rightPadding +
                        3 +
                        tp.width +
                        _textFramePadding * 2,
                    dy + _textFramePadding + tp.height / 2),
              ),
              strokeParams,
            )
            ..drawLine(Offset(canvasSize.width - rightPadding, dy),
                Offset(canvasSize.width - rightPadding + 3, dy), paintAxes);
          tp.paint(
            canvas,
            Offset(canvasSize.width - rightPadding + 3 + _textFramePadding,
                dy - tp.height / 2),
          );

          // draw bottom focused value
          final s =
              (minX + (maxX - minX) * ((dx - leftPadding) / canvasSize.width))
                  .toStringAsFixed(axesParams.chartXAxisParams.decimals);
          final spanStrike =
              TextSpan(style: lightChartValueTrackingTextStyle, text: s);
          final tpStrike = TextPainter(
              text: spanStrike,
              textAlign: TextAlign.center,
              textDirection: TextDirection.ltr)
            ..layout();
          final double startX = min(
            canvasSize.width -
                rightPadding -
                tpStrike.width / 2 -
                _textFramePadding,
            max(dx - tpStrike.width / 2, leftPadding),
          );
          canvas
            ..drawRect(
                Rect.fromPoints(
                  Offset(startX - _textFramePadding,
                      canvasSize.height - bottomPadding + 4),
                  Offset(
                    startX + tpStrike.width + _textFramePadding,
                    canvasSize.height -
                        bottomPadding +
                        tpStrike.height +
                        _textFramePadding * 2,
                  ),
                ),
                fillLabel)
            ..drawRect(
                Rect.fromPoints(
                  Offset(startX - _textFramePadding,
                      canvasSize.height - bottomPadding + 4),
                  Offset(
                    startX + tpStrike.width + _textFramePadding,
                    canvasSize.height -
                        bottomPadding +
                        tpStrike.height +
                        _textFramePadding * 2,
                  ),
                ),
                strokeParams)
            ..drawLine(Offset(dx, canvasSize.height - bottomPadding),
                Offset(dx, canvasSize.height - bottomPadding + 3), paintAxes);

          tpStrike.paint(
            canvas,
            Offset(startX,
                canvasSize.height - bottomPadding + _textFramePadding + 3),
          );
        }
      }
    }
  }

  @override
  bool shouldRepaint(DevolChartPainter oldDelegate) {
    return true;
  }

  void drawText(Canvas canvas, TextSpan textSpan, Offset offset) {
    TextPainter(
        text: textSpan,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr)
      ..layout()
      ..paint(
        canvas,
        offset,
      );
  }
}
