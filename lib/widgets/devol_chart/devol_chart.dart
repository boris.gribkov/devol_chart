import 'dart:math';

import 'package:devol_chart/widgets/devol_chart/model/chart_axis_params.dart';
import 'package:devol_chart/widgets/devol_chart/model/chart_line_params.dart';
import 'package:devol_chart/widgets/devol_chart/chart_painter.dart';
import 'package:flutter/material.dart';

class DevolChart extends StatefulWidget {
  const DevolChart({
    super.key,
    required this.lines,
    this.axesParams = const ChartAxesParams(),
  });

  final List<ChartLineParams> lines;
  final ChartAxesParams axesParams;

  @override
  State<DevolChart> createState() => _DevolChartState();
}

class _DevolChartState extends State<DevolChart> {
  Point<double>? pointerPosition;

  @override
  Widget build(BuildContext context) {
    // return Column(
    //   children: [
    //     Text('Chart'),
    //     for(final line in widget.lines)
    //       for(final point in line.points)
    //         Text('point [${point.x}:${point.y}]'),
    //   ],
    // );
    return LayoutBuilder(builder: (context, constraints) {
      return Listener(
        onPointerHover: (event) {
          pointerPosition =
              Point(event.localPosition.dx, event.localPosition.dy);
          setState(() {});
        },
        child: CustomPaint(
          size: Size(constraints.maxWidth, constraints.maxHeight),
          painter: DevolChartPainter(
            lines: widget.lines,
            axesParams: widget.axesParams,
            cursor: pointerPosition,
          ),
        ),
      );
    });
  }
}
