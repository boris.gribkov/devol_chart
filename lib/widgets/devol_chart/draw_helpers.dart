part of 'chart_painter.dart';

final titleTextColor = Colors.red;

void drawHorizontalGridLineWithText(
  Canvas canvas,
  Rect chartEdges,
  double y,
  TextSpan span,
  Paint paintAxes,
  int decimals,
  int textPadding, {
  bool dashed = true,
}) {
  if (dashed) {
    drawHorizontalDashedLine(canvas, chartEdges, y, 4);
  } else {
    canvas.drawLine(
      Offset(chartEdges.left, y),
      Offset(chartEdges.right, y),
      paintAxes,
    );
  }

  // Draw a little line after chart before text
  canvas.drawLine(
    Offset(chartEdges.width, y),
    Offset(chartEdges.width + 3, y),
    paintAxes,
  );

  final textPainter = TextPainter(
      text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr)
    ..layout();
  textPainter.paint(
      canvas,
      Offset(
        chartEdges.width + 8,
        y - textPainter.height * 0.6, // Align text to center of line
      ));
}

void drawVerticalGridLineWithText(
  Canvas canvas,
  Rect chartEdges,
  double x,
  TextSpan span,
  Paint paintAxes,
  int decimals,
  int textPadding, {
  bool dashed = true,
}) {
  if (dashed) {
    drawVerticalDashedLine(canvas, chartEdges, x, 4);
  } else {
    canvas.drawLine(
      Offset(x, chartEdges.top),
      Offset(x, chartEdges.bottom),
      paintAxes,
    );
  }

  // Draw a little line after chart before text
  canvas.drawLine(
    Offset(x, chartEdges.bottom),
    Offset(x, chartEdges.bottom + 3),
    paintAxes,
  );

  final textPainter = TextPainter(
      text: span, textAlign: TextAlign.left, textDirection: TextDirection.ltr)
    ..layout();
  textPainter.paint(
      canvas,
      Offset(
        x - textPainter.width / 2,// Align text to center of line
        chartEdges.bottom + 8,
      ));
}

void drawHorizontalDashedLine(Canvas canvas, Rect borders, double y, int step) {
  final paint = Paint()
    ..color = titleTextColor
    ..strokeWidth = 1
    ..strokeCap = StrokeCap.round;
  final offsetList = List<Offset>.generate(
    borders.width ~/ step,
    (i) => Offset((i * step).toDouble(), y),
  );
  canvas.drawPoints(PointMode.points, offsetList, paint);
}

void drawVerticalDashedLine(Canvas canvas, Rect borders, double x, int step) {
  final paint = Paint()
    ..color = titleTextColor
    ..strokeWidth = 1
    ..strokeCap = StrokeCap.round;
  final offsetList = List<Offset>.generate(
    borders.height ~/ step,
    (i) => Offset(x, (i * step).toDouble()),
  );
  canvas.drawPoints(PointMode.points, offsetList, paint);
}

void drawHorizontDashed(Canvas canvas, Size size, double y, int step) {
  final paint = Paint()
    ..color = titleTextColor
    ..strokeWidth = 1
    ..strokeCap = StrokeCap.round;
  final offsetList = List<Offset>.generate(
      (size.width - leftPadding - rightPadding) ~/ step,
      (i) => Offset(leftPadding + (i * step).toDouble(), y));
  canvas.drawPoints(PointMode.points, offsetList, paint);
}

void drawVerticalDashed(Canvas canvas, Size size, double x, int step) {
  final paint = Paint()
    ..color = titleTextColor
    ..strokeWidth = 1
    ..strokeCap = StrokeCap.round;
  final offsetList = List<Offset>.generate(
      (size.height - topPadding - bottomPadding) ~/ step,
      (i) => Offset(x, topPadding + (i * step).toDouble()));
  canvas.drawPoints(PointMode.points, offsetList, paint);
}

void drawTracking(Canvas canvas, double dx, double dy, Size size) {
  const step = 2;
  final paint = Paint()
    ..color = Colors.white
    ..strokeWidth = 1
    ..strokeCap = StrokeCap.round;
  var offsetList = List<Offset>.generate(
      (size.width - leftPadding - rightPadding) ~/ step,
      (i) => Offset(leftPadding + (i * step).toDouble(), dy));
  canvas.drawPoints(PointMode.points, offsetList, paint);
  offsetList = List.generate((size.height - topPadding - bottomPadding) ~/ step,
      (i) => Offset(dx, topPadding + (i * step).toDouble()));
  canvas.drawPoints(PointMode.points, offsetList, paint);
}
