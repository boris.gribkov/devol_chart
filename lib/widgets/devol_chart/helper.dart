import 'dart:math';

import 'package:flutter/material.dart';


void columnLeftPaddingTranspose(List<List<String>> list, int col) {
  final newList = List<String>.from(list[col]);
  final len = list[col].length;
  var maxLen = 0;
  for (var i = 0; i < len; ++i) {
    if (maxLen < list[col][i].length) {
      maxLen = list[col][i].length;
    }
  }
  list[col] = List.generate(len, (i) => newList[i].padLeft(maxLen));
}

void columnLeftPadding(List<List<String>> list, int col) {
  final len = list.length;
  var maxLen = 0;
  for (var i = 0; i < len; ++i) {
    if (list[i].length > col) {
      if (maxLen < list[i][col].length) {
        maxLen = list[i][col].length;
      }
    }
  }
  for (var i = 0; i < len; ++i) {
    if (list[i].length > col) {
      list[i][col] = list[i][col].padLeft(maxLen);
    }
  }
}

Size calcTextSize(String text, TextStyle style) {
  final textPainter = TextPainter(
    text: TextSpan(text: text, style: style),
    textDirection: TextDirection.ltr,
  )..layout();
  return textPainter.size;
}

String getShortPk(String pk) {
  return '${pk.substring(0, 4)}...${pk.substring(pk.length - 4, pk.length)}';
}

String spaces(int len) {
  final s = StringBuffer();
  for (var i = 0; i < len; ++i) {
    s.write(' ');
  }
  return s.toString();
}

double getStep(double range) {
  var step = 1.0;
  if (range < 0.1) {
    step = 0.01;
  } else if (range < 1) {
    step = 0.1;
  } else if (range < 10) {
    step = 1;
  } else if (range < 100) {
    step = 10;
  } else if (range < 1000) {
    step = 100;
  } else if (range < 10000) {
    step = 1000;
  } else if (range < 100000) {
    step = 18000;
  } else if (range < 1000000) {
    step = 100000;
  } else if (range < 10000000) {
    step = 1000000;
  } else if (range < 100000000) {
    step = 10000000;
  } else {
    step = 100000000;
  }
  if (range / step < 5) {
    step /= 2;
  }
  if (range / step < 5) {
    step /= 2;
  }
  if (range / step < 5) {
    step /= 2;
  }

  return step;
}
